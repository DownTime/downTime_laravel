<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class InsertController extends Controller
{
    public function adduser(){
        if(DB::connection()->getDatabaseName())
        {
            $status = 200;
           $_POST = json_decode(file_get_contents('php://input'),true);
            if(isset($_POST) && !empty($_POST)) {
                $username = $_POST['user_id'];
                $password = $_POST['user_pass'];
                $statusUser = $_POST['user_status'];
                // $username = 'ab099dsss';
                // $password = 'password';
                // $statusUser = 1;
                $emp_id = DB::select("SELECT MAX(emp_id) FROM `employee`");
                foreach ($emp_id as $key => $value){
                    foreach ($value as $key2 => $value2){
                        $max_empID = $value2;
                    }
                }
                $emp_id2 = $max_empID + 1;
                // echo $emp_id2."<br>";
                // DB::select("INSERT INTO `employee` (`emp_id`, `EnNo`, `emp_name`, `emp_lastname`, `emp_nickname`, `emp_address`, `emp_birthday`, `emp_weight`, `emp_height`, `emp_img`, `emp_routine`, `emp_still`, `emp_data`) VALUES ($emp_id2, NULL, NULL, NULL, NULL, NULL, null, NULL, NULL, NULL, '1', '1', '1');");
                // DB::select("INSERT INTO `user` (`user_id`, `user_pass`, `user_status`, `emp_id`) VALUES ('$username', '$password', '$statusUser', '$emp_id2')");
                DB::table('employee')->insert([
                    ['emp_id' => $emp_id2, 
                    'EnNo' => null, 
                    'emp_sex' => null,
                    'emp_name' => null, 
                    'emp_lastname' => null,
                    'emp_nickname' => null,
                    'emp_address' => null,
                    'emp_birthday' => null,
                    'emp_weight' => null,
                    'emp_height' => null,
                    'emp_img' => null,
                    'emp_routine' => 1,
                    'emp_still' => 1,
                    'emp_data' => 1,]
                ]);
                DB::table('user')->insert([
                    ['user_id' => $username, 'user_pass' => $password, 'user_status' => $statusUser, 'emp_id' => $emp_id2]
                ]);
               $report = array('message' => 'เพิ่ม user สำเร็จ (add database)', 'status' => $status);
            }else {
                $status = 401;
                $report = array('message' => 'กรุณากรอกข้อมูล (not data)', 'status' => $status);
            }
        }else{
            $status = 401;
            $report = array('message' => 'ไม่สามารถติอต่อฐานข้อมูลได้ (not database)', 'status' => $status);
        }
       return response()->json($report);
    }

    public function addmanager(){
        if(DB::connection()->getDatabaseName())
        {
            $status = 200;
            $_POST = json_decode(file_get_contents('php://input'),true);
            if(isset($_POST) && !empty($_POST)) {
                $username = $_POST['user_id'];
                $password = $_POST['user_pass'];
                // $username = 'qeabbcd';
                // $password = 'password';
                $emp_id = DB::select("SELECT MAX(emp_id) FROM `employee`");
                foreach ($emp_id as $key => $value){
                    foreach ($value as $key2 => $value2){
                        $max_empID = $value2;
                    }
                }
                $emp_id2 = $max_empID + 1;
                DB::table('employee')->insert([
                    ['emp_id' => $emp_id2, 
                    'EnNo' => null, 
                    'emp_sex' => null,
                    'emp_name' => null, 
                    'emp_lastname' => null,
                    'emp_nickname' => null,
                    'emp_address' => null,
                    'emp_birthday' => null,
                    'emp_weight' => null,
                    'emp_height' => null,
                    'emp_img' => null,
                    'emp_routine' => 1,
                    'emp_still' => 1,
                    'emp_data' => 1,]
                ]);
                DB::table('user')->insert([
                    ['user_id' => $username, 'user_pass' => $password, 'user_status' => 3, 'emp_id' => $emp_id2]
                ]);
               $report = array('message' => 'เพิ่ม user สำเร็จ (add database)', 'status' => $status);
            }else {
                $status = 401;
                $report = array('message' => 'กรุณากรอกข้อมูล (not data)', 'status' => $status);
            }
        }else{
            $status = 401;
            $report = array('message' => 'ไม่สามารถติอต่อฐานข้อมูลได้ (not database)', 'status' => $status);
        }
       return response()->json($report);
    }


    public function createUser(){
        $_POST = json_decode(file_get_contents('php://input'),true);
        // print_r ($_POST);
        $username = $_POST["username"];
        $nickname = $_POST["nickname"];
        $level = $_POST["level"];
        $position = $_POST["position"];
        $status = 200;
        function RandomString()
        {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $randstring = '';
            $randstring = $characters[rand(0, strlen($characters)-1)];
            return $randstring;
        }
        $password = '';
        $rand = RandomString();
        for ($i = 0; $i < 8; $i++){
            $rand = RandomString();
            $password .= $rand;
        }
        
        $report = array('message' => 'เพิ่ม user สำเร็จ (add database)', 'status' => $status, '$_POST' => $_POST, 'password' => $password);
        return response()->json($report);
    }

    public function AddPositionsApi(){
        // $user = Auth::user();
        // return response()->json(['success' => $user], $this->successStatus);
        // $_POST = json_decode(file_get_contents('php://input'),true);
        $report = array('message' => 'เพิ่ม ตำแหน่ง สำเร็จ (add database)', 'status' => 200);
        // // return response()->json(['success' => $user], $report);
        
        return response()->json($report);
    }
}
