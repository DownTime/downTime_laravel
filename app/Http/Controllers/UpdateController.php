<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;
use DB;

class UpdateController extends Controller
{
    public function update_user(){
        if(DB::connection()->getDatabaseName())
        {
            $status = 200;
           $_POST = json_decode(file_get_contents('php://input'),true);
            if(isset($_POST) && !empty($_POST)) {
                $username = $_POST['user_id'];
                $password = $_POST['user_pass'];
                $statusUser = $_POST['user_status'];
                $emp_id = $_POST['emp_id'];
                // $username = 'ab099d';
                // $password = 'password';
                // $statusUser = 1;
                // $emp_id = 42;
                $user = Auth::user();
                $success['user'] = $user;
                $success['password'] = bcrypt($password);
                DB::table('users')
                    ->where('name', $username)
                    ->update(['password' => $success['password']]);
                
                DB::select("UPDATE `user` SET `user_id` = '$username', `user_pass` = '$password', `user_status` = '$statusUser' WHERE `user`.`emp_id` = '$emp_id'");
               $report = array('message' => 'อัพเดท user สำเร็จ (อัพเดท database)', 'status' => $status, 'success' => $success);
            }else {
                $status = 401;
                $report = array('message' => 'กรุณากรอกข้อมูล (not data)', 'status' => $status);
            }
        }else{
            $status = 401;
            $report = array('message' => 'ไม่สามารถติอต่อฐานข้อมูลได้ (not database)', 'status' => $status);
        }
       return response()->json($report);
    }

    public function addmanager(){
        if(DB::connection()->getDatabaseName())
        {
            $status = 200;
           $_POST = json_decode(file_get_contents('php://input'),true);
            if(isset($_POST) && !empty($_POST)) {
                $username = $_POST['user_id'];
                $password = $_POST['user_pass'];
                // $username = 'qeabbcd';
                // $password = 'password';
                $emp_id = DB::select("SELECT MAX(emp_id) FROM `employee`");
                foreach ($emp_id as $key => $value){
                    foreach ($value as $key2 => $value2){
                        $max_empID = $value2;
                    }
                }
                $emp_id2 = $max_empID + 1;
                DB::table('employee')->insert([
                    ['emp_id' => $emp_id2, 
                    'EnNo' => null, 
                    'emp_name' => null, 
                    'emp_lastname' => null,
                    'emp_nickname' => null,
                    'emp_address' => null,
                    'emp_birthday' => null,
                    'emp_weight' => null,
                    'emp_height' => null,
                    'emp_img' => null,
                    'emp_routine' => 1,
                    'emp_still' => 1,
                    'emp_data' => 1,]
                ]);
                DB::table('user')->insert([
                    ['user_id' => $username, 'user_pass' => $password, 'user_status' => 3, 'emp_id' => $emp_id2]
                ]);
               $report = array('message' => 'เพิ่ม user สำเร็จ (add database)', 'status' => $status);
            }else {
                $status = 401;
                $report = array('message' => 'กรุณากรอกข้อมูล (not data)', 'status' => $status);
            }
        }else{
            $status = 401;
            $report = array('message' => 'ไม่สามารถติอต่อฐานข้อมูลได้ (not database)', 'status' => $status);
        }
       return response()->json($report);
    }

    public function update_register(){
        if(DB::connection()->getDatabaseName())
        {
            $status = 200;
            $_POST = json_decode(file_get_contents('php://input'),true);
            if(isset($_POST) && !empty($_POST)) {
                $inputs = $_POST['inputs'];
                // print_r ($inputs);
                $FirstName = $inputs[0]['inputFirstName'];
                $LastName = $inputs[0]['inputLastName'];
                $NickName = $inputs[0]['inputNickName'];
                $Birthday = $inputs[0]['inputBirthday'];
                $Address = $inputs[0]['inputAddress'];
                $Weight = $inputs[0]['inputWeight'];
                $Height = $inputs[0]['inputHeight'];
                $emp_id = $inputs[0]['emp_id'];
                $inputFile = $inputs[1]['inputFile'];
                $uploadImg = $inputFile;

                $FileImg = "img/profile/$emp_id.jpg";
                // copy($uploadImg, $FileImg);

                $data = DB::select("UPDATE `employee` SET `emp_name` = '$FirstName', `emp_lastname` = '$LastName',
                 `emp_nickname` = '$NickName', `emp_address`  = '$Address'  , `emp_birthday` = '$Birthday',
                 `emp_weight`   = '$Weight'   , `emp_height`   = '$Height' , `emp_data` = 1
                 WHERE `employee`.`emp_id` = $emp_id;");
                 $status = 200;
                 $report = array('message' => 'บันทึกข้อมูลสำเร็จ', 'status' => $status);
        
            }else {
                $status = 401;
                $report = array('message' => 'กรุณากรอกข้อมูล', 'status' => $status);
            }
        }else{
            $status = 401;
            $report = array('message' => 'ไม่สามารถติอต่อฐานข้อมูลได้', 'status' => $status);
            echo "update_register";
        }
       return response()->json($report);
    }
}
